import { ExtendRegexp } from './extend-regexp'
import { Marked } from './marked'
import { Renderer } from './renderer'
import {
  RulesInlineBase,
  MarkedOptions,
  Links,
  Link,
  RulesInlineGfm,
  RulesInlineBreaks,
  RulesInlineExtra,
  RulesInlinePedantic,
  RulesInlineCallback,
  InlineRuleOption
} from './interfaces'

/**
 * Inline Lexer & Compiler.
 */
export class InlineLexer {
  static simpleRules: {
    rule: RegExp
    render: Function
    options: InlineRuleOption
  }[] = []
  protected static rulesBase: RulesInlineBase
  /**
   * Pedantic Inline Grammar.
   */
  protected static rulesPedantic: RulesInlinePedantic
  /**
   * GFM Inline Grammar
   */
  protected static rulesGfm: RulesInlineGfm
  /**
   * GFM + Line Breaks Inline Grammar.
   */
  protected static rulesBreaks: RulesInlineBreaks
  /**
   * GFM + Line Breaks + Extra Inline Grammar.
   */
  protected static rulesExtra: RulesInlineExtra
  protected rules:
    | RulesInlineBase
    | RulesInlinePedantic
    | RulesInlineGfm
    | RulesInlineBreaks
    | RulesInlineExtra
  protected renderer: Renderer
  protected inLink: boolean
  protected hasRulesGfm: boolean
  protected hasRulesExtra: boolean
  protected ruleCallbacks: RulesInlineCallback[]

  constructor(
    protected staticThis: typeof InlineLexer,
    protected links: Links = {},
    protected options: MarkedOptions = Marked.options,
    renderer?: Renderer
  ) {
    this.renderer =
      renderer || this.options.renderer || new Renderer(this.options)

    this.setRules()
  }

  /**
   * Static Lexing/Compiling Method.
   */
  static output(src: string, links: Links, options: MarkedOptions): string {
    const inlineLexer = new this(this, links, options)
    return inlineLexer.output(src)
  }

  protected static getRulesBase(): RulesInlineBase {
    if (this.rulesBase) return this.rulesBase

    /**
     * Inline-Level Grammar.
     */
    const base: RulesInlineBase = {
      escape: /^\\([\\`*{}\[\]()#+\-.!_>])/,
      autolink: /^<(scheme:[^\s\x00-\x1f<>]*|email)>/,
      tag: /^<!--[\s\S]*?-->|^<\/?[a-zA-Z0-9\-]+(?:"[^"]*"|'[^']*'|\s[^<'">\/\s]*)*?\/?>/,
      link: /^!?\[(inside)\]\(href\)/,
      reflink: /^!?\[(inside)\]\s*\[([^\]]*)\]/,
      nolink: /^!?\[((?:\[[^\[\]]*\]|\\[\[\]]|[^\[\]])*)\]/,
      strong: /^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)/,
      em: /^_([^\s_](?:[^_]|__)+?[^\s_])_\b|^\*((?:\*\*|[^*])+?)\*(?!\*)/,
      code: /^(`+)\s*([\s\S]*?[^`]?)\s*\1(?!`)/,
      br: /^ {2,}\n(?!\s*$)/,
      text: /^[\s\S]+?(?=[\\<!\[`*]|\b_| {2,}\n|$)/,
      _scheme: /[a-zA-Z][a-zA-Z0-9+.-]{1,31}/,
      _email: /[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+(@)[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+(?![-_])/,
      _inside: /(?:\[[^\[\]]*\]|\\[\[\]]|[^\[\]]|\](?=[^\[]*\]))*/,
      _href: /\s*<?([\s\S]*?)>?(?:\s+['"]([\s\S]*?)['"])?\s*/
    }

    base.autolink = new ExtendRegexp(base.autolink)
      .setGroup('scheme', base._scheme)
      .setGroup('email', base._email)
      .getRegex()

    base.link = new ExtendRegexp(base.link)
      .setGroup('inside', base._inside)
      .setGroup('href', base._href)
      .getRegex()

    base.reflink = new ExtendRegexp(base.reflink)
      .setGroup('inside', base._inside)
      .getRegex()

    return (this.rulesBase = base)
  }

  protected static getRulesPedantic(): RulesInlinePedantic {
    if (this.rulesPedantic) return this.rulesPedantic

    return (this.rulesPedantic = {
      ...this.getRulesBase(),
      ...{
        strong: /^__(?=\S)([\s\S]*?\S)__(?!_)|^\*\*(?=\S)([\s\S]*?\S)\*\*(?!\*)/,
        em: /^_(?=\S)([\s\S]*?\S)_(?!_)|^\*(?=\S)([\s\S]*?\S)\*(?!\*)/
      }
    })
  }

  protected static getRulesGfm(): RulesInlineGfm {
    if (this.rulesGfm) return this.rulesGfm

    const base = this.getRulesBase()

    const escape = new ExtendRegexp(base.escape)
      .setGroup('])', '~|])')
      .getRegex()

    const _url = /^((?:ftp|https?):\/\/|www\.)(?:[a-zA-Z0-9\-]+\.?)+[^\s<]*|^email/
    const url = new ExtendRegexp(_url)
      .setGroup('email', base._email)
      .getRegex()

    const _backpedal = /(?:[^?!.,:;*_~()&]+|\([^)]*\)|&(?![a-zA-Z0-9]+;$)|[?!.,:;*_~)]+(?!$))+/

    const del = /^~~(?=\S)([\s\S]*?\S)~~/

    const text = new ExtendRegexp(base.text)
      .setGroup(']|', '~]|')
      .setGroup(
        '|',
        "|https?://|ftp://|www\\.|[a-zA-Z0-9.!#$%&'*+/=?^_`{\\|}~-]+@|"
      )
      .getRegex()

    return (this.rulesGfm = {
      ...base,
      ...{ escape, url, _backpedal, del, text }
    })
  }

  protected static getRulesBreaks(): RulesInlineBreaks {
    if (this.rulesBreaks) return this.rulesBreaks

    const gfm = this.getRulesGfm()

    return (this.rulesBreaks = {
      ...gfm,
      ...{
        br: new ExtendRegexp(gfm.br).setGroup('{2,}', '*').getRegex(),
        text: new ExtendRegexp(gfm.text).setGroup('{2,}', '*').getRegex()
      }
    })
  }

  protected static getRulesExtra(): RulesInlineExtra {
    if (this.rulesExtra) return this.rulesExtra

    const breaks = this.getRulesBreaks()

    return (this.rulesExtra = {
      ...breaks,
      ...{
        fnref: new ExtendRegexp(/^!?\[\^(inside)\]/)
          .setGroup('inside', breaks._inside)
          .getRegex()
      }
    })
  }

  protected setRules() {
    if (this.options.extra) {
      this.rules = this.staticThis.getRulesExtra()
    } else if (this.options.gfm) {
      this.rules = this.options.breaks
        ? this.staticThis.getRulesBreaks()
        : this.staticThis.getRulesGfm()
    } else if (this.options.pedantic) {
      this.rules = this.staticThis.getRulesPedantic()
    } else {
      this.rules = this.staticThis.getRulesBase()
    }

    if (this.options.inlineSplitChars) {
      const textRuleStr = this.rules.text.toString()
      const newStr = `${this.options.inlineSplitChars}]|`

      if (!textRuleStr.includes(newStr)) {
        this.rules.text = new RegExp(
          textRuleStr.replace(']|', newStr).slice(1, -1)
        )
      }
    }

    this.options.disabledRules.forEach(
      (
        rule: keyof (
          | RulesInlineBase
          | RulesInlinePedantic
          | RulesInlineGfm
          | RulesInlineBreaks
          | RulesInlineExtra)
      ) => {
        ;(<any>this.rules)[rule] = this.options.noop
      }
    )

    this.hasRulesGfm = (<RulesInlineGfm>this.rules).url !== undefined
    this.hasRulesExtra = (<RulesInlineExtra>this.rules).fnref !== undefined
  }

  /**
   * Lexing/Compiling.
   */
  output(nextPart: string): string {
    nextPart = nextPart
    let execArr: RegExpExecArray
    let out = ''
    const preParts = [nextPart, nextPart]
    const simpleRules = this.staticThis.simpleRules || []
    const simpleRulesBefore = simpleRules.filter(
      (rule) => rule.options.priority === 'before'
    )
    const simpleRulesAfter = simpleRules.filter(
      (rule) => rule.options.priority !== 'before'
    )

    mainLoop: while (nextPart) {
      // escape
      if ((execArr = this.rules.escape.exec(nextPart))) {
        nextPart = nextPart.substring(execArr[0].length)
        out += execArr[1]
        continue
      }

      // simple rules before
      for (const sr of simpleRulesBefore) {
        if ((execArr = sr.rule.exec(nextPart))) {
          preParts[0] = preParts[1]
          preParts[1] = nextPart
          if (!sr.options.checkPreChar || sr.options.checkPreChar(preParts[0].charAt(preParts[0].length - nextPart.length - 1))) {
            nextPart = nextPart.substring(execArr[0].length)
            out += sr.render.call(this, execArr)
            continue mainLoop
          }
        }
      }

      // autolink
      if ((execArr = this.rules.autolink.exec(nextPart))) {
        let text: string, href: string
        nextPart = nextPart.substring(execArr[0].length)
        if (execArr[2] === '@') {
          text = this.options.escape(this.mangle(execArr[1]))
          href = 'mailto:' + text
        } else {
          text = this.options.escape(execArr[1])
          href = text
        }

        out += this.renderer.link(href, null, text)
        continue
      }

      // url (gfm)
      if (
        !this.inLink &&
        this.hasRulesGfm &&
        (execArr = (<RulesInlineGfm>this.rules).url.exec(nextPart))
      ) {
        let text: string, href: string
        execArr[0] = (<RulesInlineGfm>this.rules)._backpedal.exec(execArr[0])[0]
        nextPart = nextPart.substring(execArr[0].length)
        text = this.options.escape(execArr[0])
        if (execArr[2] === '@') {
          href = 'mailto:' + text
        } else {
          if (execArr[1] === 'www.') {
            href = 'http://' + text
          } else {
            href = text
          }
        }
        out += this.renderer.link(href, null, text)
        continue
      }

      // tag
      if ((execArr = this.rules.tag.exec(nextPart))) {
        if (!this.inLink && /^<a /i.test(execArr[0])) {
          this.inLink = true
        } else if (this.inLink && /^<\/a>/i.test(execArr[0])) {
          this.inLink = false
        }

        nextPart = nextPart.substring(execArr[0].length)

        out += this.options.sanitize
          ? this.options.sanitizer
            ? this.options.sanitizer.call(this, execArr[0])
            : this.options.escape(execArr[0])
          : execArr[0]
        continue
      }

      // link
      if ((execArr = this.rules.link.exec(nextPart))) {
        nextPart = nextPart.substring(execArr[0].length)
        this.inLink = true

        out += this.outputLink(execArr, {
          href: execArr[2],
          title: execArr[3]
        })

        this.inLink = false
        continue
      }

      // fnref
      if (
        this.hasRulesExtra &&
        (execArr = (<RulesInlineExtra>this.rules).fnref.exec(nextPart))
      ) {
        nextPart = nextPart.substring(execArr[0].length)
        out += this.renderer.fnref(this.options.slug(execArr[1]))
        continue
      }

      // reflink, nolink
      if (
        (execArr = this.rules.reflink.exec(nextPart)) ||
        (execArr = this.rules.nolink.exec(nextPart))
      ) {
        nextPart = nextPart.substring(execArr[0].length)
        const keyLink = (execArr[2] || execArr[1]).replace(/\s+/g, ' ')
        const link = this.links[keyLink.toLowerCase()]

        if (!link || !link.href) {
          out += execArr[0].charAt(0)
          nextPart = execArr[0].substring(1) + nextPart
          continue
        }

        this.inLink = true
        out += this.outputLink(execArr, link)
        this.inLink = false
        continue
      }

      // strong
      if ((execArr = this.rules.strong.exec(nextPart))) {
        nextPart = nextPart.substring(execArr[0].length)
        out += this.renderer.strong(this.output(execArr[2] || execArr[1]))
        continue
      }

      // em
      if ((execArr = this.rules.em.exec(nextPart))) {
        nextPart = nextPart.substring(execArr[0].length)
        out += this.renderer.em(this.output(execArr[2] || execArr[1]))
        continue
      }

      // code
      if ((execArr = this.rules.code.exec(nextPart))) {
        nextPart = nextPart.substring(execArr[0].length)
        out += this.renderer.codespan(
          this.options.escape(execArr[2].trim(), true)
        )
        continue
      }

      // br
      if ((execArr = this.rules.br.exec(nextPart))) {
        nextPart = nextPart.substring(execArr[0].length)
        out += this.renderer.br()
        continue
      }

      // del (gfm)
      if (
        this.hasRulesGfm &&
        (execArr = (<RulesInlineGfm>this.rules).del.exec(nextPart))
      ) {
        nextPart = nextPart.substring(execArr[0].length)
        out += this.renderer.del(this.output(execArr[1]))
        continue
      }

      // simple rules after
      for (const sr of simpleRulesAfter) {
        if ((execArr = sr.rule.exec(nextPart))) {
          preParts[0] = preParts[1]
          preParts[1] = nextPart
          if (!sr.options.checkPreChar || sr.options.checkPreChar(preParts[0].charAt(preParts[0].length - nextPart.length - 1))) {
            nextPart = nextPart.substring(execArr[0].length)
            out += sr.render.call(this, execArr)
            continue mainLoop
          }
        }
      }

      // text
      if ((execArr = this.rules.text.exec(nextPart))) {
        nextPart = nextPart.substring(execArr[0].length)
        out += this.renderer.text(
          this.options.escape(this.smartypants(execArr[0]))
        )
        continue
      }

      if (nextPart)
        throw new Error('Infinite loop on byte: ' + nextPart.charCodeAt(0))
    }

    return out
  }

  /**
   * Compile Link.
   */
  protected outputLink(execArr: RegExpExecArray, link: Link) {
    const href = this.options.escape(link.href)
    const title = link.title ? this.options.escape(link.title) : null

    return execArr[0].charAt(0) !== '!'
      ? this.renderer.link(href, title, this.output(execArr[1]))
      : this.renderer.image(href, title, this.options.escape(execArr[1]))
  }

  /**
   * Smartypants Transformations.
   */
  protected smartypants(text: string) {
    if (!this.options.smartypants) return text

    return (
      text
        // em-dashes
        .replace(/---/g, '\u2014')
        // en-dashes
        .replace(/--/g, '\u2013')
        // opening singles
        .replace(/(^|[-\u2014/(\[{"\s])'/g, '$1\u2018')
        // closing singles & apostrophes
        .replace(/'/g, '\u2019')
        // opening doubles
        .replace(/(^|[-\u2014/(\[{\u2018\s])"/g, '$1\u201c')
        // closing doubles
        .replace(/"/g, '\u201d')
        // ellipses
        .replace(/\.{3}/g, '\u2026')
    )
  }

  /**
   * Mangle Links.
   */
  protected mangle(text: string) {
    if (!this.options.mangle) return text

    let out = ''
    let length = text.length
    for (let i = 0; i < length; i++) {
      let ch: string | number = text.charCodeAt(i)

      if (Math.random() > 0.5) {
        ch = 'x' + ch.toString(16)
      }

      out += '&#' + ch + ';'
    }

    return out
  }
}
