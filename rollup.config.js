import typescript from 'rollup-plugin-typescript2';
import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import cleanup from 'rollup-plugin-cleanup';
import progress from 'rollup-plugin-progress';
import filesize from 'rollup-plugin-filesize';
import pkg from './package.json';

export default {
  plugins: [
    typescript({
      tsconfig: 'tsconfig.json'
    }),
    babel({
      exclude: 'node_modules/**'
    }),
    resolve(),
    commonjs(),
    cleanup({
      comments: 'none'
    }),
    progress(),
    filesize()
  ],
  input: 'src/index.ts',
  output: [
    {
      format: 'es',
      file: pkg.module
    },
    {
      format: 'cjs',
      file: pkg.main
    }
  ]
};
