import { Renderer } from './renderer';
import { RulesInlineBase, MarkedOptions, Links, Link, RulesInlineGfm, RulesInlineBreaks, RulesInlineExtra, RulesInlinePedantic, RulesInlineCallback, InlineRuleOption } from './interfaces';
/**
 * Inline Lexer & Compiler.
 */
export declare class InlineLexer {
    protected staticThis: typeof InlineLexer;
    protected links: Links;
    protected options: MarkedOptions;
    static simpleRules: {
        rule: RegExp;
        render: Function;
        options: InlineRuleOption;
    }[];
    protected static rulesBase: RulesInlineBase;
    /**
     * Pedantic Inline Grammar.
     */
    protected static rulesPedantic: RulesInlinePedantic;
    /**
     * GFM Inline Grammar
     */
    protected static rulesGfm: RulesInlineGfm;
    /**
     * GFM + Line Breaks Inline Grammar.
     */
    protected static rulesBreaks: RulesInlineBreaks;
    /**
     * GFM + Line Breaks + Extra Inline Grammar.
     */
    protected static rulesExtra: RulesInlineExtra;
    protected rules: RulesInlineBase | RulesInlinePedantic | RulesInlineGfm | RulesInlineBreaks | RulesInlineExtra;
    protected renderer: Renderer;
    protected inLink: boolean;
    protected hasRulesGfm: boolean;
    protected hasRulesExtra: boolean;
    protected ruleCallbacks: RulesInlineCallback[];
    constructor(staticThis: typeof InlineLexer, links?: Links, options?: MarkedOptions, renderer?: Renderer);
    /**
     * Static Lexing/Compiling Method.
     */
    static output(src: string, links: Links, options: MarkedOptions): string;
    protected static getRulesBase(): RulesInlineBase;
    protected static getRulesPedantic(): RulesInlinePedantic;
    protected static getRulesGfm(): RulesInlineGfm;
    protected static getRulesBreaks(): RulesInlineBreaks;
    protected static getRulesExtra(): RulesInlineExtra;
    protected setRules(): void;
    /**
     * Lexing/Compiling.
     */
    output(nextPart: string): string;
    /**
     * Compile Link.
     */
    protected outputLink(execArr: RegExpExecArray, link: Link): string;
    /**
     * Smartypants Transformations.
     */
    protected smartypants(text: string): string;
    /**
     * Mangle Links.
     */
    protected mangle(text: string): string;
}
