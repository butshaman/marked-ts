export declare function escape(html: string, encode?: boolean): string;
export declare function unescape(html: string): string;
export declare function slug(str: string): string;
export declare function resolveUrl(base: string, href: string): string;
export declare function noop(): void;
