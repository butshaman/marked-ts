import { Renderer, TextRenderer } from './renderer';
import { InlineLexer } from './inline-lexer';
import { MarkedOptions, Token, Links, SimpleRenderer } from './interfaces';
/**
 * Parsing & Compiling.
 */
export declare class Parser {
    simpleRenderers: SimpleRenderer[];
    protected tokens: Token[];
    protected token: Token;
    protected footnotes: {
        [key: string]: string;
    };
    protected inlineLexer: InlineLexer;
    protected inlineTextLexer: InlineLexer;
    protected options: MarkedOptions;
    protected renderer: Renderer;
    protected textRenderer: TextRenderer;
    protected line: number;
    constructor(options?: MarkedOptions);
    static parse(tokens: Token[], links: Links, options?: MarkedOptions): string;
    parse(links: Links, tokens: Token[]): string;
    debug(links: Links, tokens: Token[]): string;
    protected next(): Token;
    protected getNextElement(): Token;
    protected parseText(): string;
    protected tok(): any;
}
