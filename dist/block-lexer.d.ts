import { RulesBlockBase, MarkedOptions, Token, Links, LexerReturns, RulesBlockGfm, RulesBlockTables, RulesBlockExtra } from './interfaces';
export declare class BlockLexer<T extends typeof BlockLexer> {
    protected staticThis: typeof BlockLexer;
    static simpleRules: RegExp[];
    protected static rulesBase: RulesBlockBase;
    /**
     * GFM Block Grammar.
     */
    protected static rulesGfm: RulesBlockGfm;
    /**
     * GFM + Tables Block Grammar.
     */
    protected static rulesTables: RulesBlockTables;
    /**
     * GFM + Tables + Extra Block Grammar.
     */
    protected static rulesExtra: RulesBlockExtra;
    protected rules: RulesBlockBase | RulesBlockGfm | RulesBlockTables | RulesBlockExtra;
    protected options: MarkedOptions;
    protected links: Links;
    protected tokens: Token[];
    protected hasRulesGfm: boolean;
    protected hasRulesTables: boolean;
    protected hasRulesExtra: boolean;
    constructor(staticThis: typeof BlockLexer, options?: object);
    /**
     * Accepts Markdown text and returns object with tokens and links.
     *
     * @param src String of markdown source to be compiled.
     * @param options Hash of options.
     */
    static lex(src: string, options?: MarkedOptions, top?: boolean): LexerReturns;
    protected static getRulesBase(): RulesBlockBase;
    protected static getRulesGfm(): RulesBlockGfm;
    protected static getRulesTable(): RulesBlockTables;
    protected static getRulesExtra(): RulesBlockExtra;
    protected setRules(): void;
    /**
     * Lexing.
     */
    protected getTokens(src: string, top?: boolean): LexerReturns;
}
