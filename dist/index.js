'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

var __assign = Object.assign || function __assign(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
    }
    return t;
};

var Renderer = /** @class */function () {
    function Renderer(options) {
        this.options = options || Marked.options;
        this._headings = [];
        this._footnotes = [];
    }
    Renderer.prototype.code = function (code, lang, escaped) {
        if (this.options.highlight) {
            var out = this.options.highlight(code, lang);
            if (out != null && out !== code) {
                escaped = true;
                code = out;
            }
        }
        if (!lang) {
            return "\n<pre><code>" + (escaped ? code : this.options.escape(code, true)) + "\n</code></pre>\n";
        }
        var dataLang = this.options.langAttribute ? " data-lang=\"" + this.options.escape(lang, true) + "\"" : '';
        return "\n<pre" + dataLang + "><code class=\"" + this.options.langPrefix + this.options.escape(lang, true) + "\">" + (escaped ? code : this.options.escape(code, true)) + "\n</code></pre>\n";
    };
    Renderer.prototype.blockquote = function (quote) {
        return "<blockquote>\n" + quote + "</blockquote>\n";
    };
    Renderer.prototype.html = function (html) {
        return html;
    };
    Renderer.prototype.heading = function (text, level, raw, ends) {
        var headerId = this.options.headerId;
        var idHtml = '';
        if (!headerId || headerId === 'off' && ends || headerId === 'on' && !ends) {
            var id = this.options.slug(raw);
            var count = this._headings.filter(function (h) {
                return h === raw;
            }).length;
            if (count > 0) {
                id += "-" + count;
            }
            idHtml = " id=\"" + this.options.headerPrefix + id + "\"";
            this._headings.push(raw);
        }
        return "<h" + level + idHtml + ">" + text + "</h" + level + ">\n";
    };
    Renderer.prototype.hr = function () {
        return this.options.xhtml ? '<hr/>\n' : '<hr>\n';
    };
    Renderer.prototype.list = function (body, ordered, isTaskList) {
        var type = ordered ? 'ol' : 'ul';
        var classNames = isTaskList ? ' class="task-list"' : '';
        return "\n<" + type + classNames + ">\n" + body + "</" + type + ">\n";
    };
    Renderer.prototype.listitem = function (text, checked) {
        if (checked === null) {
            return "<li>" + text + "</li>\n";
        }
        return "<li class=\"task-list-item\"><input type=\"checkbox\" class=\"task-list-item-checkbox\" " + (checked ? 'checked ' : '') + " disabled> " + text + "</li>\n";
    };
    Renderer.prototype.paragraph = function (text) {
        return "<p>" + text + "</p>\n";
    };
    Renderer.prototype.table = function (header, body) {
        return "\n<table>\n<thead>\n" + header + "</thead>\n<tbody>\n" + body + "</tbody>\n</table>\n";
    };
    Renderer.prototype.tablerow = function (content) {
        return "<tr>\n" + content + "</tr>\n";
    };
    Renderer.prototype.tablecell = function (content, flags) {
        var type = flags.header ? 'th' : 'td';
        var tag = flags.align ? '<' + type + ' style="text-align:' + flags.align + '">' : '<' + type + '>';
        return tag + content + '</' + type + '>\n';
    };
    //*** Inline level renderer methods. ***
    Renderer.prototype.strong = function (text) {
        return "<strong>" + text + "</strong>";
    };
    Renderer.prototype.em = function (text) {
        return "<em>" + text + "</em>";
    };
    Renderer.prototype.codespan = function (text) {
        return "<code>" + text + "</code>";
    };
    Renderer.prototype.br = function () {
        return this.options.xhtml ? '<br/>' : '<br>';
    };
    Renderer.prototype.del = function (text) {
        return "<del>" + text + "</del>";
    };
    Renderer.prototype.fnref = function (refname) {
        if (!this._footnotes.includes(refname)) {
            this._footnotes.push(refname);
        }
        return "<sup id=\"fnref:" + refname + "\"><a href=\"#fn:" + refname + "\" class=\"footnote-ref\" role=\"doc-noteref\">" + this._footnotes.length + "</a></sup>";
    };
    Renderer.prototype.footnote = function (footnotes) {
        var out = "<div class=\"footnotes\" role=\"doc-endnotes\">" + this.hr() + "<ol>";
        for (var _i = 0, _a = this._footnotes; _i < _a.length; _i++) {
            var refname = _a[_i];
            out += "<li id=\"fn:" + refname + "\" role=\"doc-endnote\"><span class=\"cite-text\">" + (footnotes[refname] || '?') + "</span><a href=\"#fnref:" + refname + "\" class=\"footnote-backref\" role=\"doc-backlink\">&#8617;</a></li>";
        }
        out += '</ol></div>';
        this._footnotes = [];
        return out;
    };
    Renderer.prototype.link = function (href, title, text) {
        if (this.options.sanitize) {
            var prot = void 0;
            try {
                prot = decodeURIComponent(this.options.unescape(href)).replace(/[^\w:]/g, '').toLowerCase();
            } catch (e) {
                return text;
            }
            if (prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0 || prot.indexOf('data:') === 0) {
                return text;
            }
        }
        if (this.options.baseUrl) {
            href = this.options.resolveUrl(this.options.baseUrl, href);
        }
        var out = '<a href="' + href + '"';
        if (title) {
            out += ' title="' + title + '"';
        }
        var _a = this.options,
            linksInNewTab = _a.linksInNewTab,
            trimLinkText = _a.trimLinkText;
        var targetBlank = linksInNewTab === true || typeof linksInNewTab === 'function' && linksInNewTab.call(this, href);
        if (typeof targetBlank === 'string') {
            out += targetBlank;
        } else if (targetBlank) {
            out += " target=\"_blank\"";
        }
        if (trimLinkText) {
            text = trimLinkText(text);
        }
        out += '>' + text + '</a>';
        return out;
    };
    Renderer.prototype.image = function (href, title, text) {
        if (this.options.baseUrl) {
            href = this.options.resolveUrl(this.options.baseUrl, href);
        }
        var out = '<img src="' + href + '" alt="' + text + '"';
        if (title) {
            out += ' title="' + title + '"';
        }
        out += this.options.xhtml ? '/>' : '>';
        return out;
    };
    Renderer.prototype.text = function (text) {
        return text;
    };
    return Renderer;
}();
var TextRenderer = /** @class */function () {
    function TextRenderer() {}
    TextRenderer.prototype.strong = function (text) {
        return text;
    };
    TextRenderer.prototype.em = function (text) {
        return text;
    };
    TextRenderer.prototype.codespan = function (text) {
        return text;
    };
    TextRenderer.prototype.del = function (text) {
        return text;
    };
    TextRenderer.prototype.text = function (text) {
        return text;
    };
    TextRenderer.prototype.link = function (href, title, text) {
        return '' + text;
    };
    TextRenderer.prototype.image = function (href, title, text) {
        return '' + text;
    };
    TextRenderer.prototype.br = function () {
        return '';
    };
    return TextRenderer;
}();

var ExtendRegexp = /** @class */function () {
    function ExtendRegexp(regex, flags) {
        if (flags === void 0) {
            flags = '';
        }
        this.source = regex.source;
        this.flags = flags;
    }
    /**
     * Extend regular expression.
     *
     * @param groupName Regular expression for search a group name.
     * @param groupRegexp Regular expression of named group.
     */
    ExtendRegexp.prototype.setGroup = function (groupName, groupRegexp) {
        var newRegexp = typeof groupRegexp == 'string' ? groupRegexp : groupRegexp.source;
        newRegexp = newRegexp.replace(/(^|[^\[])\^/g, '$1');
        // Extend regexp.
        this.source = this.source.replace(groupName, newRegexp);
        return this;
    };
    /**
     * Returns a result of extending a regular expression.
     */
    ExtendRegexp.prototype.getRegex = function () {
        return new RegExp(this.source, this.flags);
    };
    return ExtendRegexp;
}();

/**
 * Inline Lexer & Compiler.
 */
var InlineLexer = /** @class */function () {
    function InlineLexer(staticThis, links, options, renderer) {
        if (links === void 0) {
            links = {};
        }
        if (options === void 0) {
            options = Marked.options;
        }
        this.staticThis = staticThis;
        this.links = links;
        this.options = options;
        this.renderer = renderer || this.options.renderer || new Renderer(this.options);
        this.setRules();
    }
    /**
     * Static Lexing/Compiling Method.
     */
    InlineLexer.output = function (src, links, options) {
        var inlineLexer = new this(this, links, options);
        return inlineLexer.output(src);
    };
    InlineLexer.getRulesBase = function () {
        if (this.rulesBase) return this.rulesBase;
        /**
         * Inline-Level Grammar.
         */
        var base = {
            escape: /^\\([\\`*{}\[\]()#+\-.!_>])/,
            autolink: /^<(scheme:[^\s\x00-\x1f<>]*|email)>/,
            tag: /^<!--[\s\S]*?-->|^<\/?[a-zA-Z0-9\-]+(?:"[^"]*"|'[^']*'|\s[^<'">\/\s]*)*?\/?>/,
            link: /^!?\[(inside)\]\(href\)/,
            reflink: /^!?\[(inside)\]\s*\[([^\]]*)\]/,
            nolink: /^!?\[((?:\[[^\[\]]*\]|\\[\[\]]|[^\[\]])*)\]/,
            strong: /^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)/,
            em: /^_([^\s_](?:[^_]|__)+?[^\s_])_\b|^\*((?:\*\*|[^*])+?)\*(?!\*)/,
            code: /^(`+)\s*([\s\S]*?[^`]?)\s*\1(?!`)/,
            br: /^ {2,}\n(?!\s*$)/,
            text: /^[\s\S]+?(?=[\\<!\[`*]|\b_| {2,}\n|$)/,
            _scheme: /[a-zA-Z][a-zA-Z0-9+.-]{1,31}/,
            _email: /[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+(@)[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+(?![-_])/,
            _inside: /(?:\[[^\[\]]*\]|\\[\[\]]|[^\[\]]|\](?=[^\[]*\]))*/,
            _href: /\s*<?([\s\S]*?)>?(?:\s+['"]([\s\S]*?)['"])?\s*/
        };
        base.autolink = new ExtendRegexp(base.autolink).setGroup('scheme', base._scheme).setGroup('email', base._email).getRegex();
        base.link = new ExtendRegexp(base.link).setGroup('inside', base._inside).setGroup('href', base._href).getRegex();
        base.reflink = new ExtendRegexp(base.reflink).setGroup('inside', base._inside).getRegex();
        return this.rulesBase = base;
    };
    InlineLexer.getRulesPedantic = function () {
        if (this.rulesPedantic) return this.rulesPedantic;
        return this.rulesPedantic = __assign({}, this.getRulesBase(), {
            strong: /^__(?=\S)([\s\S]*?\S)__(?!_)|^\*\*(?=\S)([\s\S]*?\S)\*\*(?!\*)/,
            em: /^_(?=\S)([\s\S]*?\S)_(?!_)|^\*(?=\S)([\s\S]*?\S)\*(?!\*)/
        });
    };
    InlineLexer.getRulesGfm = function () {
        if (this.rulesGfm) return this.rulesGfm;
        var base = this.getRulesBase();
        var escape = new ExtendRegexp(base.escape).setGroup('])', '~|])').getRegex();
        var _url = /^((?:ftp|https?):\/\/|www\.)(?:[a-zA-Z0-9\-]+\.?)+[^\s<]*|^email/;
        var url = new ExtendRegexp(_url).setGroup('email', base._email).getRegex();
        var _backpedal = /(?:[^?!.,:;*_~()&]+|\([^)]*\)|&(?![a-zA-Z0-9]+;$)|[?!.,:;*_~)]+(?!$))+/;
        var del = /^~~(?=\S)([\s\S]*?\S)~~/;
        var text = new ExtendRegexp(base.text).setGroup(']|', '~]|').setGroup('|', "|https?://|ftp://|www\\.|[a-zA-Z0-9.!#$%&'*+/=?^_`{\\|}~-]+@|").getRegex();
        return this.rulesGfm = __assign({}, base, { escape: escape, url: url, _backpedal: _backpedal, del: del, text: text });
    };
    InlineLexer.getRulesBreaks = function () {
        if (this.rulesBreaks) return this.rulesBreaks;
        var gfm = this.getRulesGfm();
        return this.rulesBreaks = __assign({}, gfm, {
            br: new ExtendRegexp(gfm.br).setGroup('{2,}', '*').getRegex(),
            text: new ExtendRegexp(gfm.text).setGroup('{2,}', '*').getRegex()
        });
    };
    InlineLexer.getRulesExtra = function () {
        if (this.rulesExtra) return this.rulesExtra;
        var breaks = this.getRulesBreaks();
        return this.rulesExtra = __assign({}, breaks, {
            fnref: new ExtendRegexp(/^!?\[\^(inside)\]/).setGroup('inside', breaks._inside).getRegex()
        });
    };
    InlineLexer.prototype.setRules = function () {
        var _this = this;
        if (this.options.extra) {
            this.rules = this.staticThis.getRulesExtra();
        } else if (this.options.gfm) {
            this.rules = this.options.breaks ? this.staticThis.getRulesBreaks() : this.staticThis.getRulesGfm();
        } else if (this.options.pedantic) {
            this.rules = this.staticThis.getRulesPedantic();
        } else {
            this.rules = this.staticThis.getRulesBase();
        }
        if (this.options.inlineSplitChars) {
            var textRuleStr = this.rules.text.toString();
            var newStr = this.options.inlineSplitChars + "]|";
            if (!textRuleStr.includes(newStr)) {
                this.rules.text = new RegExp(textRuleStr.replace(']|', newStr).slice(1, -1));
            }
        }
        this.options.disabledRules.forEach(function (rule) {
            _this.rules[rule] = _this.options.noop;
        });
        this.hasRulesGfm = this.rules.url !== undefined;
        this.hasRulesExtra = this.rules.fnref !== undefined;
    };
    /**
     * Lexing/Compiling.
     */
    InlineLexer.prototype.output = function (nextPart) {
        nextPart = nextPart;
        var execArr;
        var out = '';
        var preParts = [nextPart, nextPart];
        var simpleRules = this.staticThis.simpleRules || [];
        var simpleRulesBefore = simpleRules.filter(function (rule) {
            return rule.options.priority === 'before';
        });
        var simpleRulesAfter = simpleRules.filter(function (rule) {
            return rule.options.priority !== 'before';
        });
        mainLoop: while (nextPart) {
            // escape
            if (execArr = this.rules.escape.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                out += execArr[1];
                continue;
            }
            // simple rules before
            for (var _i = 0, simpleRulesBefore_1 = simpleRulesBefore; _i < simpleRulesBefore_1.length; _i++) {
                var sr = simpleRulesBefore_1[_i];
                if (execArr = sr.rule.exec(nextPart)) {
                    preParts[0] = preParts[1];
                    preParts[1] = nextPart;
                    if (!sr.options.checkPreChar || sr.options.checkPreChar(preParts[0].charAt(preParts[0].length - nextPart.length - 1))) {
                        nextPart = nextPart.substring(execArr[0].length);
                        out += sr.render.call(this, execArr);
                        continue mainLoop;
                    }
                }
            }
            // autolink
            if (execArr = this.rules.autolink.exec(nextPart)) {
                var text = void 0,
                    href = void 0;
                nextPart = nextPart.substring(execArr[0].length);
                if (execArr[2] === '@') {
                    text = this.options.escape(this.mangle(execArr[1]));
                    href = 'mailto:' + text;
                } else {
                    text = this.options.escape(execArr[1]);
                    href = text;
                }
                out += this.renderer.link(href, null, text);
                continue;
            }
            // url (gfm)
            if (!this.inLink && this.hasRulesGfm && (execArr = this.rules.url.exec(nextPart))) {
                var text = void 0,
                    href = void 0;
                execArr[0] = this.rules._backpedal.exec(execArr[0])[0];
                nextPart = nextPart.substring(execArr[0].length);
                text = this.options.escape(execArr[0]);
                if (execArr[2] === '@') {
                    href = 'mailto:' + text;
                } else {
                    if (execArr[1] === 'www.') {
                        href = 'http://' + text;
                    } else {
                        href = text;
                    }
                }
                out += this.renderer.link(href, null, text);
                continue;
            }
            // tag
            if (execArr = this.rules.tag.exec(nextPart)) {
                if (!this.inLink && /^<a /i.test(execArr[0])) {
                    this.inLink = true;
                } else if (this.inLink && /^<\/a>/i.test(execArr[0])) {
                    this.inLink = false;
                }
                nextPart = nextPart.substring(execArr[0].length);
                out += this.options.sanitize ? this.options.sanitizer ? this.options.sanitizer.call(this, execArr[0]) : this.options.escape(execArr[0]) : execArr[0];
                continue;
            }
            // link
            if (execArr = this.rules.link.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                this.inLink = true;
                out += this.outputLink(execArr, {
                    href: execArr[2],
                    title: execArr[3]
                });
                this.inLink = false;
                continue;
            }
            // fnref
            if (this.hasRulesExtra && (execArr = this.rules.fnref.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                out += this.renderer.fnref(this.options.slug(execArr[1]));
                continue;
            }
            // reflink, nolink
            if ((execArr = this.rules.reflink.exec(nextPart)) || (execArr = this.rules.nolink.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                var keyLink = (execArr[2] || execArr[1]).replace(/\s+/g, ' ');
                var link = this.links[keyLink.toLowerCase()];
                if (!link || !link.href) {
                    out += execArr[0].charAt(0);
                    nextPart = execArr[0].substring(1) + nextPart;
                    continue;
                }
                this.inLink = true;
                out += this.outputLink(execArr, link);
                this.inLink = false;
                continue;
            }
            // strong
            if (execArr = this.rules.strong.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                out += this.renderer.strong(this.output(execArr[2] || execArr[1]));
                continue;
            }
            // em
            if (execArr = this.rules.em.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                out += this.renderer.em(this.output(execArr[2] || execArr[1]));
                continue;
            }
            // code
            if (execArr = this.rules.code.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                out += this.renderer.codespan(this.options.escape(execArr[2].trim(), true));
                continue;
            }
            // br
            if (execArr = this.rules.br.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                out += this.renderer.br();
                continue;
            }
            // del (gfm)
            if (this.hasRulesGfm && (execArr = this.rules.del.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                out += this.renderer.del(this.output(execArr[1]));
                continue;
            }
            // simple rules after
            for (var _a = 0, simpleRulesAfter_1 = simpleRulesAfter; _a < simpleRulesAfter_1.length; _a++) {
                var sr = simpleRulesAfter_1[_a];
                if (execArr = sr.rule.exec(nextPart)) {
                    preParts[0] = preParts[1];
                    preParts[1] = nextPart;
                    if (!sr.options.checkPreChar || sr.options.checkPreChar(preParts[0].charAt(preParts[0].length - nextPart.length - 1))) {
                        nextPart = nextPart.substring(execArr[0].length);
                        out += sr.render.call(this, execArr);
                        continue mainLoop;
                    }
                }
            }
            // text
            if (execArr = this.rules.text.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                out += this.renderer.text(this.options.escape(this.smartypants(execArr[0])));
                continue;
            }
            if (nextPart) throw new Error('Infinite loop on byte: ' + nextPart.charCodeAt(0));
        }
        return out;
    };
    /**
     * Compile Link.
     */
    InlineLexer.prototype.outputLink = function (execArr, link) {
        var href = this.options.escape(link.href);
        var title = link.title ? this.options.escape(link.title) : null;
        return execArr[0].charAt(0) !== '!' ? this.renderer.link(href, title, this.output(execArr[1])) : this.renderer.image(href, title, this.options.escape(execArr[1]));
    };
    /**
     * Smartypants Transformations.
     */
    InlineLexer.prototype.smartypants = function (text) {
        if (!this.options.smartypants) return text;
        return text.replace(/---/g, '\u2014').replace(/--/g, '\u2013').replace(/(^|[-\u2014/(\[{"\s])'/g, '$1\u2018').replace(/'/g, '\u2019').replace(/(^|[-\u2014/(\[{\u2018\s])"/g, '$1\u201c').replace(/"/g, '\u201d').replace(/\.{3}/g, '\u2026');
    };
    /**
     * Mangle Links.
     */
    InlineLexer.prototype.mangle = function (text) {
        if (!this.options.mangle) return text;
        var out = '';
        var length = text.length;
        for (var i = 0; i < length; i++) {
            var ch = text.charCodeAt(i);
            if (Math.random() > 0.5) {
                ch = 'x' + ch.toString(16);
            }
            out += '&#' + ch + ';';
        }
        return out;
    };
    InlineLexer.simpleRules = [];
    return InlineLexer;
}();

var escapeTest = /[&<>"']/;
var escapeReplace = /[&<>"']/g;
var replacements = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;'
};
var escapeTestNoEncode = /[<>"']|&(?!#?\w+;)/;
var escapeReplaceNoEncode = /[<>"']|&(?!#?\w+;)/g;
function escape(html, encode) {
    if (encode) {
        if (escapeTest.test(html)) {
            return html.replace(escapeReplace, function (ch) {
                return replacements[ch];
            });
        }
    } else {
        if (escapeTestNoEncode.test(html)) {
            return html.replace(escapeReplaceNoEncode, function (ch) {
                return replacements[ch];
            });
        }
    }
    return html;
}
function unescape(html) {
    // Explicitly match decimal, hex, and named HTML entities
    return html.replace(/&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/gi, function (_, n) {
        n = n.toLowerCase();
        if (n === 'colon') return ':';
        if (n.charAt(0) === '#') {
            return n.charAt(1) === 'x' ? String.fromCharCode(parseInt(n.substring(2), 16)) : String.fromCharCode(+n.substring(1));
        }
        return '';
    });
}
var regHtmlTags = /<(?:.|\n)*?>/gm;
var regSpecialChars = /[!\"#$%&'\(\)\*\+,\/:;<=>\?\@\[\\\]\^`\{\|\}~]/g;
var regDotSpace = /(\s|\.)/g;
function slug(str) {
    return str.replace(regHtmlTags, '').replace(regSpecialChars, '').replace(regDotSpace, '-').toLowerCase();
}
var originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i;
var noLastSlashUrl = /^[^:]+:\/*[^/]*$/;
var baseUrls = {};
function resolveUrl(base, href) {
    if (originIndependentUrl.test(href)) {
        return href;
    }
    var baseUrlsKey = ' ' + base;
    if (!baseUrls[baseUrlsKey]) {
        // we can ignore everything in base after the last slash of its path component,
        // but we might need to add _that_
        // https://tools.ietf.org/html/rfc3986#section-3
        if (noLastSlashUrl.test(base)) {
            baseUrls[baseUrlsKey] = base + '/';
        } else {
            baseUrls[baseUrlsKey] = base.replace(/[^/]*$/, '');
        }
    }
    base = baseUrls[baseUrlsKey];
    if (href.slice(0, 2) === '//') {
        return base.replace(/:[\s\S]*/, ':') + href;
    } else if (href.charAt(0) === '/') {
        return base.replace(/(:\/*[^/]*)[\s\S]*/, '$1') + href;
    } else {
        return base + href;
    }
}
function noop() {}
noop.exec = noop;

(function (TokenType) {
  TokenType[TokenType["space"] = 1] = "space";
  TokenType[TokenType["text"] = 2] = "text";
  TokenType[TokenType["paragraph"] = 3] = "paragraph";
  TokenType[TokenType["heading"] = 4] = "heading";
  TokenType[TokenType["listStart"] = 5] = "listStart";
  TokenType[TokenType["listEnd"] = 6] = "listEnd";
  TokenType[TokenType["looseItemStart"] = 7] = "looseItemStart";
  TokenType[TokenType["looseItemEnd"] = 8] = "looseItemEnd";
  TokenType[TokenType["listItemStart"] = 9] = "listItemStart";
  TokenType[TokenType["listItemEnd"] = 10] = "listItemEnd";
  TokenType[TokenType["blockquoteStart"] = 11] = "blockquoteStart";
  TokenType[TokenType["blockquoteEnd"] = 12] = "blockquoteEnd";
  TokenType[TokenType["code"] = 13] = "code";
  TokenType[TokenType["table"] = 14] = "table";
  TokenType[TokenType["html"] = 15] = "html";
  TokenType[TokenType["hr"] = 16] = "hr";
  TokenType[TokenType["footnote"] = 17] = "footnote";
})(exports.TokenType || (exports.TokenType = {}));
var MarkedOptions = /** @class */function () {
  function MarkedOptions() {
    this.gfm = true;
    this.tables = true;
    this.extra = false;
    this.breaks = false;
    this.pedantic = false;
    this.sanitize = false;
    this.mangle = true;
    this.smartLists = false;
    this.silent = false;
    this.baseUrl = null;
    this.linksInNewTab = false;
    this.disabledRules = [];
    this.langPrefix = 'lang-';
    this.langAttribute = false;
    this.smartypants = false;
    this.headerId = '';
    this.headerPrefix = '';
    /**
     * Self-close the tags for void elements (&lt;br/&gt;, &lt;img/&gt;, etc.)
     * with a "/" as required by XHTML.
     */
    this.xhtml = false;
    /**
     * The function that will be using to escape HTML entities.
     * By default using inner helper.
     */
    this.escape = escape;
    /**
     * The function that will be using to unescape HTML entities.
     * By default using inner helper.
     */
    this.unescape = unescape;
    /**
     * The function that will be using to slug string.
     * By default using inner helper.
     */
    this.slug = slug;
    /**
     * The RegExp that will be using to make RegExp.exec as noop.
     * By default using inner helper.
     */
    this.noop = noop;
    /**
     * The function that will be using to render image/link URLs relative to a base url.
     * By default using inner helper.
     */
    this.resolveUrl = resolveUrl;
  }
  return MarkedOptions;
}();

/**
 * Parsing & Compiling.
 */
var Parser = /** @class */function () {
    function Parser(options) {
        this.simpleRenderers = [];
        this.line = 0;
        this.tokens = [];
        this.token = null;
        this.footnotes = {};
        this.options = options || Marked.options;
        this.renderer = this.options.renderer || new Renderer(this.options);
    }
    Parser.parse = function (tokens, links, options) {
        var parser = new this(options);
        return parser.parse(links, tokens);
    };
    Parser.prototype.parse = function (links, tokens) {
        this.inlineLexer = new InlineLexer(InlineLexer, links, this.options, this.renderer);
        this.inlineTextLexer = new InlineLexer(InlineLexer, links, Object.assign({}, this.options, {
            renderer: new TextRenderer()
        }));
        this.tokens = tokens.reverse();
        var out = '';
        while (this.next()) {
            out += this.tok();
        }
        if (Object.keys(this.footnotes).length) {
            out += this.renderer.footnote(this.footnotes);
            this.footnotes = {};
        }
        // Remove cached
        this.renderer._headings = [];
        return out;
    };
    Parser.prototype.debug = function (links, tokens) {
        this.inlineLexer = new InlineLexer(InlineLexer, links, this.options, this.renderer);
        this.inlineTextLexer = new InlineLexer(InlineLexer, links, Object.assign({}, this.options, {
            renderer: new TextRenderer()
        }));
        this.tokens = tokens.reverse();
        var out = '';
        while (this.next()) {
            var outToken = this.tok();
            this.token.line = this.line += outToken.split('\n').length - 1;
            out += outToken;
        }
        if (Object.keys(this.footnotes).length) {
            out += this.renderer.footnote(this.footnotes);
            this.footnotes = {};
        }
        // Remove cached
        this.renderer._headings = [];
        return out;
    };
    Parser.prototype.next = function () {
        return this.token = this.tokens.pop();
    };
    Parser.prototype.getNextElement = function () {
        return this.tokens[this.tokens.length - 1];
    };
    Parser.prototype.parseText = function () {
        var body = this.token.text;
        var nextElement;
        while ((nextElement = this.getNextElement()) && nextElement.type == exports.TokenType.text) {
            body += '\n' + this.next().text;
        }
        return this.inlineLexer.output(body);
    };
    Parser.prototype.tok = function () {
        switch (this.token.type) {
            case exports.TokenType.space:
                {
                    return '';
                }
            case exports.TokenType.paragraph:
                {
                    return this.renderer.paragraph(this.inlineLexer.output(this.token.text));
                }
            case exports.TokenType.text:
                {
                    if (this.options.isNoP) return this.parseText();else return this.renderer.paragraph(this.parseText());
                }
            case exports.TokenType.heading:
                {
                    return this.renderer.heading(this.inlineLexer.output(this.token.text), this.token.depth, this.options.unescape(this.inlineTextLexer.output(this.token.text)), this.token.ends);
                }
            case exports.TokenType.listStart:
                {
                    var body = '',
                        ordered = this.token.ordered,
                        isTaskList = false;
                    while (this.next().type != exports.TokenType.listEnd) {
                        if (this.token.checked !== null) {
                            isTaskList = true;
                        }
                        body += this.tok();
                    }
                    return this.renderer.list(body, ordered, isTaskList);
                }
            case exports.TokenType.listItemStart:
                {
                    var body = '';
                    var checked = this.token.checked;
                    while (this.next().type != exports.TokenType.listItemEnd) {
                        body += this.token.type == exports.TokenType.text ? this.parseText() : this.tok();
                    }
                    return this.renderer.listitem(body, checked);
                }
            case exports.TokenType.looseItemStart:
                {
                    var body = '';
                    var checked = this.token.checked;
                    while (this.next().type != exports.TokenType.listItemEnd) {
                        body += this.tok();
                    }
                    return this.renderer.listitem(body, checked);
                }
            case exports.TokenType.footnote:
                {
                    this.footnotes[this.token.refname] = this.inlineLexer.output(this.token.text);
                    return '';
                }
            case exports.TokenType.code:
                {
                    return this.renderer.code(this.token.text, this.token.lang, this.token.escaped);
                }
            case exports.TokenType.table:
                {
                    var header = '',
                        body = '',
                        row = void 0,
                        cell = void 0;
                    // header
                    cell = '';
                    for (var i = 0; i < this.token.header.length; i++) {
                        var flags = { header: true, align: this.token.align[i] };
                        var out = this.inlineLexer.output(this.token.header[i]);
                        cell += this.renderer.tablecell(out, flags);
                    }
                    header += this.renderer.tablerow(cell);
                    for (var i = 0; i < this.token.cells.length; i++) {
                        row = this.token.cells[i];
                        cell = '';
                        for (var j = 0; j < row.length; j++) {
                            cell += this.renderer.tablecell(this.inlineLexer.output(row[j]), {
                                header: false,
                                align: this.token.align[j]
                            });
                        }
                        body += this.renderer.tablerow(cell);
                    }
                    return this.renderer.table(header, body);
                }
            case exports.TokenType.blockquoteStart:
                {
                    var body = '';
                    while (this.next().type != exports.TokenType.blockquoteEnd) {
                        body += this.tok();
                    }
                    return this.renderer.blockquote(body);
                }
            case exports.TokenType.hr:
                {
                    return this.renderer.hr();
                }
            case exports.TokenType.html:
                {
                    var html = !this.token.pre && !this.options.pedantic ? this.inlineLexer.output(this.token.text) : this.token.text;
                    return this.renderer.html(html);
                }
            default:
                {
                    for (var i = 0; i < this.simpleRenderers.length; i++) {
                        if (this.token.type === 'simpleRule' + (i + 1)) {
                            return this.simpleRenderers[i].call(this.renderer, this.token.execArr);
                        }
                    }
                    var errMsg = "Token with \"" + this.token.type + "\" type was not found.";
                    if (this.options.silent) {
                        console.log(errMsg);
                    } else {
                        throw new Error(errMsg);
                    }
                }
        }
    };
    return Parser;
}();

var Marked = /** @class */function () {
    function Marked() {}
    /**
     * Merges the default options with options that will be set.
     *
     * @param options Hash of options.
     */
    Marked.setOptions = function (options) {
        Object.assign(this.options, options);
        return this;
    };
    /**
     * Setting simple block rule.
     */
    Marked.setBlockRule = function (regexp, renderer) {
        if (renderer === void 0) {
            renderer = function () {
                return '';
            };
        }
        BlockLexer.simpleRules.push(regexp);
        this.simpleRenderers.push(renderer);
        return this;
    };
    /**
     * Setting simple inline rule.
     */
    Marked.setInlineRule = function (regexp, renderer, options) {
        if (options === void 0) {
            options = {};
        }
        InlineLexer.simpleRules.push({
            rule: regexp,
            render: renderer,
            options: options
        });
        return this;
    };
    /**
     * Accepts Markdown text and returns text in HTML format.
     *
     * @param src String of markdown source to be compiled.
     * @param options Hash of options. They replace, but do not merge with the default options.
     * If you want the merging, you can to do this via `Marked.setOptions()`.
     */
    Marked.inlineParse = function (src, options) {
        if (options === void 0) {
            options = this.options;
        }
        return new InlineLexer(InlineLexer, {}, options).output(src);
    };
    /**
     * Accepts Markdown text and returns text in HTML format.
     *
     * @param src String of markdown source to be compiled.
     * @param options Hash of options. They replace, but do not merge with the default options.
     * If you want the merging, you can to do this via `Marked.setOptions()`.
     */
    Marked.parse = function (src, options) {
        if (options === void 0) {
            options = this.options;
        }
        try {
            var _a = this.callBlockLexer(src, options),
                tokens = _a.tokens,
                links = _a.links;
            return this.callParser(tokens, links, options);
        } catch (e) {
            return this.callMe(e);
        }
    };
    /**
     * Accepts Markdown text and returns object with text in HTML format,
     * tokens and links from `BlockLexer.parser()`.
     *
     * @param src String of markdown source to be compiled.
     * @param options Hash of options. They replace, but do not merge with the default options.
     * If you want the merging, you can to do this via `Marked.setOptions()`.
     */
    Marked.debug = function (src, options) {
        if (options === void 0) {
            options = this.options;
        }
        var _a = this.callBlockLexer(src, options),
            tokens = _a.tokens,
            links = _a.links;
        var origin = tokens.slice();
        var parser = new Parser(options);
        parser.simpleRenderers = this.simpleRenderers;
        var result = parser.debug(links, tokens);
        /**
         * Translates a token type into a readable form,
         * and moves `line` field to a first place in a token object.
         */
        origin = origin.map(function (token) {
            token.type = exports.TokenType[token.type] || token.type;
            var line = token.line;
            delete token.line;
            if (line) return __assign({ line: line }, token);else return token;
        });
        return { tokens: origin, links: links, result: result };
    };
    Marked.callBlockLexer = function (src, options) {
        if (src === void 0) {
            src = '';
        }
        if (typeof src != 'string') throw new Error("Expected that the 'src' parameter would have a 'string' type, got '" + typeof src + "'");
        // Preprocessing.
        src = src.replace(/\r\n|\r/g, '\n').replace(/\t/g, '    ').replace(/\u00a0/g, ' ').replace(/\u2424/g, '\n').replace(/^ +$/gm, '');
        return BlockLexer.lex(src, options, true);
    };
    Marked.callParser = function (tokens, links, options) {
        if (this.simpleRenderers.length) {
            var parser = new Parser(options);
            parser.simpleRenderers = this.simpleRenderers;
            return parser.parse(links, tokens);
        } else {
            return Parser.parse(tokens, links, options);
        }
    };
    Marked.callMe = function (err) {
        if (this.options.silent) {
            return '<p>An error occurred:</p><pre>' + this.options.escape(err.message + '', true) + '</pre>';
        }
        throw err;
    };
    Marked.options = new MarkedOptions();
    Marked.simpleRenderers = [];
    return Marked;
}();

var BlockLexer = /** @class */function () {
    function BlockLexer(staticThis, options) {
        this.staticThis = staticThis;
        this.links = {};
        this.tokens = [];
        this.options = options || Marked.options;
        this.setRules();
    }
    /**
     * Accepts Markdown text and returns object with tokens and links.
     *
     * @param src String of markdown source to be compiled.
     * @param options Hash of options.
     */
    BlockLexer.lex = function (src, options, top) {
        var lexer = new this(this, options);
        return lexer.getTokens(src, top);
    };
    BlockLexer.getRulesBase = function () {
        if (this.rulesBase) return this.rulesBase;
        var base = {
            newline: /^\n+/,
            code: /^( {4}[^\n]+\n*)+/,
            hr: /^ {0,3}((?:- *){3,}|(?:_ *){3,}|(?:\* *){3,})(?:\n+|$)/,
            heading: /^ *(#{1,6}) *([^\n]+?) *(#*) *(?:\n+|$)/,
            blockquote: /^( {0,3}> ?(paragraph|[^\n]*)(?:\n|$))+/,
            list: /^( *)(bull) [\s\S]+?(?:hr|def|\n{2,}(?! )(?!\1bull )\n*|\s*$)/,
            html: /^ *(?:comment *(?:\n|\s*$)|closed *(?:\n{2,}|\s*$)|closing *(?:\n{2,}|\s*$))/,
            def: /^ {0,3}\[(label)\]: *\n? *<?([^\s>]+)>?(?:(?: +\n? *| *\n *)(title))? *(?:\n+|$)/,
            lheading: /^([^\n]+)\n *(=|-){2,} *(?:\n+|$)/,
            paragraph: /^([^\n]+(?:\n?(?!hr|heading|lheading| {0,3}>|tag)[^\n]+)+)/,
            text: /^[^\n]+/,
            bullet: /(?:[*+-]|\d+\.)/,
            item: /^( *)(bull) [^\n]*(?:\n(?!\1bull )[^\n]*)*/,
            _label: /(?:\\[\[\]]|[^\[\]])+/,
            _title: /(?:"(?:\\"|[^"]|"[^"\n]*")*"|'\n?(?:[^'\n]+\n?)*'|\([^()]*\))/
        };
        base.def = new ExtendRegexp(base.def).setGroup('label', base._label).setGroup('title', base._title).getRegex();
        base.item = new ExtendRegexp(base.item, 'gm').setGroup(/bull/g, base.bullet).getRegex();
        base.list = new ExtendRegexp(base.list).setGroup(/bull/g, base.bullet).setGroup('hr', '\\n+(?=\\1?(?:(?:- *){3,}|(?:_ *){3,}|(?:\\* *){3,})(?:\\n+|$))').setGroup('def', '\\n+(?=' + base.def.source + ')').getRegex();
        var tag = '(?!(?:' + 'a|em|strong|small|s|cite|q|dfn|abbr|data|time|code' + '|var|samp|kbd|sub|sup|i|b|u|mark|ruby|rt|rp|bdi|bdo' + '|span|br|wbr|ins|del|img)\\b)\\w+(?!:|[^\\w\\s@]*@)\\b';
        base.html = new ExtendRegexp(base.html).setGroup('comment', /<!--[\s\S]*?-->/).setGroup('closed', /<(tag)[\s\S]+?<\/\1>/).setGroup('closing', /<tag(?:"[^"]*"|'[^']*'|\s[^'"\/>\s]*)*?\/?>/).setGroup(/tag/g, tag).getRegex();
        base.paragraph = new ExtendRegexp(base.paragraph).setGroup('hr', base.hr).setGroup('heading', base.heading).setGroup('lheading', base.lheading).setGroup('blockquote', base.blockquote).setGroup('tag', '<' + tag).getRegex();
        base.blockquote = new ExtendRegexp(base.blockquote).setGroup('paragraph', base.paragraph).getRegex();
        return this.rulesBase = base;
    };
    BlockLexer.getRulesGfm = function () {
        if (this.rulesGfm) return this.rulesGfm;
        var base = this.getRulesBase();
        var gfm = __assign({}, base, {
            fences: /^ *(`{3,}|~{3,})[ \.]*(\S+)? *\n([\s\S]*?)\n? *\1 *(?:\n+|$)/,
            checkbox: /^\[([ x])\] +/,
            paragraph: /^/,
            heading: /^ *(#{1,6}) +([^\n]+?) *(#*) *(?:\n+|$)/
        });
        var group1 = gfm.fences.source.replace('\\1', '\\2');
        var group2 = base.list.source.replace('\\1', '\\3');
        gfm.paragraph = new ExtendRegexp(base.paragraph).setGroup('(?!', "(?!" + group1 + "|" + group2 + "|").getRegex();
        return this.rulesGfm = gfm;
    };
    BlockLexer.getRulesTable = function () {
        if (this.rulesTables) return this.rulesTables;
        return this.rulesTables = __assign({}, this.getRulesGfm(), {
            nptable: /^ *(\S.*\|.*)\n *([-:]+ *\|[-| :]*)\n((?:.*\|.*(?:\n|$))*)\n*/,
            table: /^ *\|(.+)\n *\|( *[-:]+[-| :]*)\n((?: *\|.*(?:\n|$))*)\n*/
        });
    };
    BlockLexer.getRulesExtra = function () {
        if (this.rulesExtra) return this.rulesExtra;
        var table = this.getRulesTable();
        table.paragraph = new ExtendRegexp(table.paragraph).setGroup('footnote', /^\[\^([^\]]+)\]: *([^\n]*(?:\n+|$)(?: {1,}[^\n]*(?:\n+|$))*)/).getRegex();
        return this.rulesExtra = __assign({}, table, { footnote: /^\[\^([^\]]+)\]: ([^\n]+)/ });
    };
    BlockLexer.prototype.setRules = function () {
        var _this = this;
        if (this.options.extra) {
            this.rules = this.staticThis.getRulesExtra();
        } else if (this.options.gfm) {
            if (this.options.tables) {
                this.rules = this.staticThis.getRulesTable();
            } else {
                this.rules = this.staticThis.getRulesGfm();
            }
        } else {
            this.rules = this.staticThis.getRulesBase();
        }
        this.options.disabledRules.forEach(function (rule) {
            _this.rules[rule] = _this.options.noop;
        });
        this.hasRulesGfm = this.rules.fences !== undefined;
        this.hasRulesTables = this.rules.table !== undefined;
        this.hasRulesExtra = this.rules.footnote !== undefined;
    };
    /**
     * Lexing.
     */
    BlockLexer.prototype.getTokens = function (src, top) {
        var nextPart = src;
        var execArr;
        mainLoop: while (nextPart) {
            // newline
            if (execArr = this.rules.newline.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                if (execArr[0].length > 1) {
                    this.tokens.push({
                        type: exports.TokenType.space
                    });
                }
            }
            // code
            if (execArr = this.rules.code.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                var code = execArr[0].replace(/^ {4}/gm, '');
                this.tokens.push({
                    type: exports.TokenType.code,
                    text: !this.options.pedantic ? code.replace(/\n+$/, '') : code
                });
                continue;
            }
            // fences code (gfm)
            if (this.hasRulesGfm && (execArr = this.rules.fences.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                this.tokens.push({
                    type: exports.TokenType.code,
                    lang: execArr[2],
                    text: execArr[3] || ''
                });
                continue;
            }
            // footnote
            if (this.hasRulesExtra && (execArr = this.rules.footnote.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                var item = {
                    type: exports.TokenType.footnote,
                    refname: this.options.slug(execArr[1]),
                    text: execArr[2]
                };
                this.tokens.push(item);
                continue;
            }
            // heading
            if (execArr = this.rules.heading.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                this.tokens.push({
                    type: exports.TokenType.heading,
                    depth: execArr[1].length,
                    text: execArr[2],
                    ends: execArr[3] || ''
                });
                continue;
            }
            // table no leading pipe (gfm)
            if (top && this.hasRulesTables && (execArr = this.rules.nptable.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                var item = {
                    type: exports.TokenType.table,
                    header: execArr[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
                    align: execArr[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
                    cells: []
                };
                for (var i = 0; i < item.align.length; i++) {
                    if (/^ *-+: *$/.test(item.align[i])) {
                        item.align[i] = 'right';
                    } else if (/^ *:-+: *$/.test(item.align[i])) {
                        item.align[i] = 'center';
                    } else if (/^ *:-+ *$/.test(item.align[i])) {
                        item.align[i] = 'left';
                    } else {
                        item.align[i] = null;
                    }
                }
                var td = execArr[3].replace(/\n$/, '').split('\n');
                for (var i = 0; i < td.length; i++) {
                    item.cells[i] = td[i].split(/ *\| */);
                }
                this.tokens.push(item);
                continue;
            }
            // hr
            if (execArr = this.rules.hr.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                this.tokens.push({
                    type: exports.TokenType.hr
                });
                continue;
            }
            // blockquote
            if (execArr = this.rules.blockquote.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                this.tokens.push({
                    type: exports.TokenType.blockquoteStart
                });
                var str = execArr[0].replace(/^ *> ?/gm, '');
                // Pass `top` to keep the current
                // "toplevel" state. This is exactly
                // how markdown.pl works.
                this.getTokens(str);
                this.tokens.push({
                    type: exports.TokenType.blockquoteEnd
                });
                continue;
            }
            // list
            if (execArr = this.rules.list.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                var bull = execArr[2];
                this.tokens.push({
                    type: exports.TokenType.listStart,
                    ordered: bull.length > 1
                });
                // Get each top-level item.
                var str = execArr[0].match(this.rules.item);
                var length_1 = str.length;
                var next = false,
                    space = void 0,
                    blockBullet = void 0,
                    loose = void 0;
                for (var i = 0; i < length_1; i++) {
                    var item = str[i];
                    var checked = null;
                    // Remove the list item's bullet, so it is seen as the next token.
                    space = item.length;
                    item = item.replace(/^ *([*+-]|\d+\.) +/, '');
                    if (this.hasRulesGfm && (execArr = this.rules.checkbox.exec(item))) {
                        checked = execArr[1] === 'x';
                        item = item.replace(this.rules.checkbox, '');
                    }
                    // Outdent whatever the list item contains. Hacky.
                    if (item.indexOf('\n ') !== -1) {
                        space -= item.length;
                        item = !this.options.pedantic ? item.replace(new RegExp('^ {1,' + space + '}', 'gm'), '') : item.replace(/^ {1,4}/gm, '');
                    }
                    // Determine whether the next list item belongs here.
                    // Backpedal if it does not belong in this list.
                    if (this.options.smartLists && i !== length_1 - 1) {
                        blockBullet = this.staticThis.getRulesBase().bullet.exec(str[i + 1])[0];
                        if (bull !== blockBullet && !(bull.length > 1 && blockBullet.length > 1)) {
                            nextPart = str.slice(i + 1).join('\n') + nextPart;
                            i = length_1 - 1;
                        }
                    }
                    // Determine whether item is loose or not.
                    // Use: /(^|\n)(?! )[^\n]+\n\n(?!\s*$)/
                    // for discount behavior.
                    loose = next || /\n\n(?!\s*$)/.test(item);
                    if (i !== length_1 - 1) {
                        next = item.charAt(item.length - 1) === '\n';
                        if (!loose) loose = next;
                    }
                    this.tokens.push({
                        checked: checked,
                        type: loose ? exports.TokenType.looseItemStart : exports.TokenType.listItemStart
                    });
                    // Recurse.
                    this.getTokens(item, false);
                    this.tokens.push({
                        type: exports.TokenType.listItemEnd
                    });
                }
                this.tokens.push({
                    type: exports.TokenType.listEnd
                });
                continue;
            }
            // html
            if (execArr = this.rules.html.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                var attr = execArr[1];
                var isPre = attr === 'pre' || attr === 'script' || attr === 'style';
                this.tokens.push({
                    type: this.options.sanitize ? exports.TokenType.paragraph : exports.TokenType.html,
                    pre: !this.options.sanitizer && isPre,
                    text: execArr[0]
                });
                continue;
            }
            // def
            if (top && (execArr = this.rules.def.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                var tag = execArr[1].toLowerCase();
                if (!this.links[tag]) {
                    var title = execArr[3];
                    if (title) {
                        title = title.substring(1, title.length - 1);
                    }
                    this.links[tag] = { title: title, href: execArr[2] };
                }
                continue;
            }
            // table (gfm)
            if (top && this.hasRulesTables && (execArr = this.rules.table.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                var item = {
                    type: exports.TokenType.table,
                    header: execArr[1].replace(/^ *| *\| *$/g, '').split(/ *\| */),
                    align: execArr[2].replace(/^ *|\| *$/g, '').split(/ *\| */),
                    cells: []
                };
                for (var i = 0; i < item.align.length; i++) {
                    if (/^ *-+: *$/.test(item.align[i])) {
                        item.align[i] = 'right';
                    } else if (/^ *:-+: *$/.test(item.align[i])) {
                        item.align[i] = 'center';
                    } else if (/^ *:-+ *$/.test(item.align[i])) {
                        item.align[i] = 'left';
                    } else {
                        item.align[i] = null;
                    }
                }
                var td = execArr[3].replace(/(?: *\| *)?\n$/, '').split('\n');
                for (var i = 0; i < td.length; i++) {
                    item.cells[i] = td[i].replace(/^ *\| *| *\| *$/g, '').split(/ *\| */);
                }
                this.tokens.push(item);
                continue;
            }
            // simple rules
            if (this.staticThis.simpleRules.length) {
                var simpleRules = this.staticThis.simpleRules;
                for (var i = 0; i < simpleRules.length; i++) {
                    if (execArr = simpleRules[i].exec(nextPart)) {
                        nextPart = nextPart.substring(execArr[0].length);
                        var type = 'simpleRule' + (i + 1);
                        this.tokens.push({
                            type: type,
                            execArr: execArr
                        });
                        continue mainLoop;
                    }
                }
            }
            // lheading
            if (execArr = this.rules.lheading.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                this.tokens.push({
                    type: exports.TokenType.heading,
                    depth: execArr[2] === '=' ? 1 : 2,
                    text: execArr[1]
                });
                continue;
            }
            // top-level paragraph
            if (top && (execArr = this.rules.paragraph.exec(nextPart))) {
                nextPart = nextPart.substring(execArr[0].length);
                if (execArr[1].slice(-1) === '\n') {
                    this.tokens.push({
                        type: exports.TokenType.paragraph,
                        text: execArr[1].slice(0, -1)
                    });
                } else {
                    this.tokens.push({
                        type: this.tokens.length > 0 ? exports.TokenType.paragraph : exports.TokenType.text,
                        text: execArr[1]
                    });
                }
                continue;
            }
            // text
            // Top-level should never reach here.
            if (execArr = this.rules.text.exec(nextPart)) {
                nextPart = nextPart.substring(execArr[0].length);
                this.tokens.push({
                    type: exports.TokenType.text,
                    text: execArr[0]
                });
                continue;
            }
            if (nextPart) {
                throw new Error('Infinite loop on byte: ' + nextPart.charCodeAt(0) + (", near text '" + nextPart.slice(0, 30) + "...'"));
            }
        }
        return { tokens: this.tokens, links: this.links };
    };
    BlockLexer.simpleRules = [];
    return BlockLexer;
}();

/**
 * @license
 *
 * Copyright (c) 2011-2014, Christopher Jeffrey. (MIT Licensed)
 * https://github.com/chjj/marked
 *
 * Copyright (c) 2018, Костя Третяк. (MIT Licensed)
 * https://github.com/KostyaTretyak/marked-ts
 *
 * Copyright (c) 2018, Yahtnif. (MIT Licensed)
 * https://bitbucket.org/butshaman/marked-ts
 */

exports.BlockLexer = BlockLexer;
exports.escape = escape;
exports.unescape = unescape;
exports.slug = slug;
exports.resolveUrl = resolveUrl;
exports.noop = noop;
exports.InlineLexer = InlineLexer;
exports.MarkedOptions = MarkedOptions;
exports.Marked = Marked;
exports.Parser = Parser;
exports.Renderer = Renderer;
exports.TextRenderer = TextRenderer;
exports.ExtendRegexp = ExtendRegexp;
