import { MarkedOptions, Align } from './interfaces';
export declare class Renderer {
    protected options: MarkedOptions;
    _headings: string[];
    _footnotes: string[];
    constructor(options?: MarkedOptions);
    code(code: string, lang?: string, escaped?: boolean): string;
    blockquote(quote: string): string;
    html(html: string): string;
    heading(text: string, level: number, raw: string, ends: string): string;
    hr(): string;
    list(body: string, ordered?: boolean, isTaskList?: boolean): string;
    listitem(text: string, checked?: boolean | null): string;
    paragraph(text: string): string;
    table(header: string, body: string): string;
    tablerow(content: string): string;
    tablecell(content: string, flags: {
        header?: boolean;
        align?: Align;
    }): string;
    strong(text: string): string;
    em(text: string): string;
    codespan(text: string): string;
    br(): string;
    del(text: string): string;
    fnref(refname: string): string;
    footnote(footnotes: {
        [key: string]: string;
    }): string;
    link(href: string, title: string, text: string): string;
    image(href: string, title: string, text: string): string;
    text(text: string): string;
}
export declare class TextRenderer {
    strong(text: string): string;
    em(text: string): string;
    codespan(text: string): string;
    del(text: string): string;
    text(text: string): string;
    link(href: string, title: string, text: string): string;
    image(href: string, title: string, text: string): string;
    br(): string;
}
